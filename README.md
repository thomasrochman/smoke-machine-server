# Smoke Machine Slave Server #

## Requirements: ##
* Python 3.5+
* pysimpleDMX-0.2.0
* python-osc
* argparse

### Installation Notes ###
In order to install pysimpleDMX:   
Insert the following code into your command line and run:

```
#!bash

pip install pysimpledmx
pip install git+https://github.com/c0z3n/pySimpleDMX.git
pip install git+https://github.com/tobypatterson/pySimpleDMX.git
```
   
in order to install python-osc:   
Insert the following code into your command line and run:

```
#!bash

pip install python-osc
```
### Running Server ###

To run the server:   
cd into the directory containing the smokeMachine.py file and run the following from command line:

```
#!bash

python smokeMachine.py <channels_of_machines> [-p <USBDMXPRO_address>] [-i <server_ip_address>] [-s <server_port>]
```
Replace <channels_of_machines> with ordered channel numbers of smoke machines separated by spaces.   
Replace <server_ip_address> with the ip of the client the server will be listening to.   
Replace <server_port> with the port of the server.   
   
Optional   
With -p flag: replace <USBDMXPRO_address> with port address of the USB DMX PRO controller. (For Windows, in the form 'COM*', where * is the port number)   
With -i flag: replace <server_ip_address> with the ip address the server listens to.   
With -s flag: replace <server_port> with the port address of the client.   
   
## Operations ##
The Available commands are:   
### /update ###
Updates the DMX frame and changes the values of the smoke machines
attached. The values should be passed by ordered list of ints separated through a message:

```
#!python

client.send_message('/update', [int, int, int, ...])
```

### /clear ###
Clears the DMX frame and changes all values to 0. To call, send a message to the server of the form:

```
#!python

client.send_message('/clear', <dummy_value>)
```
Replace <dummy_value> with any given primitive data type.
### /change_channel ###
Use to change the channel of a given machine in case of input error or channel change. Call by client message:

```
#!python

client.send_message('/change_channel', [int machine_index, int new_channel])
```
machine_index is replaced with the index of the smoke machine in order as initiated when running the server. new_channel is replaced by the int channel value of the DMX machine.