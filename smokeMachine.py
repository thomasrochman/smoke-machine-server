# DMX Smoke Machines

from pysimpledmx import pysimpledmx
import glob
from pythonosc import osc_server, dispatcher
import argparse
import sys
import traceback

def auto_update(unused_address, args, *vals):
	'''
	obj args[0] = mydmx
	list args[1] = channels
	list vals = vals for ordered channels 
	'''
	print(vals)
	print(type(vals[0]))
	print('update called')
	try:
		for i in range(len(vals)):
			if vals[i] is not None:
				args[0].setChannel(args[1][i], max(0, int(min(255 * int(vals[i]) / 100, 255))))
			print(args[0].dmx_frame)
		args[0].render()
		print("Updated values:", vals)
	except:
		print(traceback.format_exc())
		print("Problem updating values. Skipping process.")

def clear(unused_address, args, *unused_input):
	'''
	obj args[0] = mydmx
	'''
	print('clear called')
	try:
		args[0].clear()
		args[0].render()
		print("Cleared")
	except:
		print(traceback.format_exc())
		print("Unable to clear DMX frame. Skipping process.")

def terminate(unused_address, args, *unused_input):
	'''
	server args[0] = server_kill
	'''
	print('terminate called')
	try:
		args[0].kill_server()
		print("Server shutdown")
	except:
		print(traceback.format_exc())
		print("Unable to shutdown server. Skipping process.")

def change_channel(unused_address, args, *vals):
	'''
	list args[0] = channels
	obj args[1] = mydmx
	int vals[0] = channel_number
	int vals[1] = new_channel_port
	'''
	if len(vals) != 2:
		raise Exception("Incorrect Number of Variables")
	print('change_channel called')
	previous = args[0][vals[0]]
	args[0][vals[0]] = int(vals[1]) + 1
	temp = args[1].dmx_frame[vals[1]]
	args[1].dmx_frame[int(vals[1]) + 1] = args[1].dmx_frame[previous]

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Begin smoke machine server')
	parser.add_argument('channels', type=int, nargs='+', help='DMX channel starting addresses in order of reference')
	parser.add_argument('-p', '--port_address', dest='port_address', type=str, default=None, help='Port address of DMX machine')
	parser.add_argument('-i', '--ip', dest='ip', type=str, default="127.0.0.1", help='The ip of the OSC server')
	parser.add_argument('-s', '--server_port', dest='port_listen', type=int, default=5005, help='The port the OSC server is listening on')

	
	args = parser.parse_args()
	port = args.port_address
	channels = [x + 1 for x in args.channels] # +1 for each


	if port is None:

		if sys.platform.startswith("win"):
			from serial.tools import list_ports
			try:
				port = next(list_ports.grep("USB Serial Port")) # Subject to Change
			except StopIteration:
				raise Exception("No device found.")

		elif sys.platform.startswith("linux") or sys.platform.startswith('cygwin'):
			ports = glob.glob('/dev/cu[A-Za-z]*')
			if len(ports) > 1:
				num = int(input("Choose port to connect reader to by index: \n %s \n" % str(ports)))
				port = ports[num]
			if len(ports) < 1:
				raise Exception('Device not connected')
			else:
				port = ports[0]

		elif sys.platform.startswith("darwin"):
			ports = glob.glob('/dev/cu.usbserial-*')
			if len(ports) > 1:
				num = int(input("Choose port to connect reader to by index: \n %s \n" % str(ports)))
				port = ports[num]
			if len(ports) < 1:
				raise Exception('Device not connected')
			else:
				port = ports[0]

		else:
			raise EnvironmentError('Unsupported Platform')
	else:
		port = args.port_address

	print("Connecting to", port)

	mydmx = pysimpledmx.DMXConnection(port)
	print("Success")
	dispatcher = dispatcher.Dispatcher()
	dispatcher.map('/update', auto_update, mydmx, channels)
	dispatcher.map('/clear', clear, mydmx)
	# dispatcher.map('/close', terminate, server_kill)
	dispatcher.map('/change_channel', change_channel, channels, mydmx)
	server = osc_server.ForkingOSCUDPServer((args.ip, args.port_listen), dispatcher)
	try:
		server.serve_forever()
	except KeyboardInterrupt:
		pass
	server.server_close()
	print("Exiting")