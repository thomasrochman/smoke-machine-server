# simple_smoke_client.py

import argparse

from pythonosc import osc_message_builder

from pythonosc import udp_client

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("--ip", default="127.0.0.1", help="The ip of the OSC server")
	parser.add_argument("--port", type=int, default=5005,help="The port the OSC server is listening on")
	args = parser.parse_args()
	client = udp_client.SimpleUDPClient(args.ip, args.port)
	command = ''

	while command != "/exit":
		command = input('Command:')
		if command == 'exit':
			break
		parameters = input('Parameters:')
		if command == '/update':
			print(type(parameters))
			parlist = parameters.split(' ')
			parameters = [int(i) for i in parlist]
			print(parameters)
		client.send_message(command, parameters)

	print("Process complete")